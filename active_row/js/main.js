(function ($) {
  Drupal.behaviors.active_row = {
    attach: function (context, settings) {
     
      $('body').once('active_row').each(function () {
               
        var nid = getParameterByName('nid');

		$('.views-field-nid').each(function(){
			var compare = $(this).html().replace(/ /g,'');
			if(compare == nid) {
				$(this).parent().addClass('active');
			}
		});
        
      });

      	function getParameterByName(name) {
		    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
		    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
		}
        
    }
  };
})(jQuery);