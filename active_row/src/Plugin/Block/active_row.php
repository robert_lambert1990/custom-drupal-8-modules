<?php

namespace Drupal\active_row\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'active_row' block.
 *
 * @Block(
 *  id = "active_row",
 *  admin_label = @Translation("Active_row"),
 * )
 */
class active_row extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

	return array(
	'#theme' => 'active_row',
	'#test_var' => 'Test Value',
	'#attached' => array(
	  'library' => array('active_row/active_row'),
	  ),
	);

	
  }

}
