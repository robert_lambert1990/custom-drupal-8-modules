<?php

/**
 * @file
 * Contains domain_limited_categories.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function domain_limited_categories_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the domain_limited_categories module.
    case 'help.page.domain_limited_categories':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Limits the categories available on the article module') . '</p>';
      return $output;

    default:
  }
}

/**
 *  Implements hook_form_alter()
 */
function domain_limited_categories_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if($form_id == 'node_article_form' or $form_id == 'node_article_edit_form') {
    _domain_limited_categories_replace_categories($form);
    _domain_limited_categories_set_default_department($form);
    _domain_limited_categories_fieldset($form);    
  }
}

function _domain_get_users_department() {
    $uid = \Drupal::currentUser()->id();
    $user = user_load($uid);
    $department = $user->field_department->getValue();
    return $department;
}

function _domain_limited_categories_set_default_department(&$form) {
    $department = _domain_get_users_department();
    $department_id = $department[0]['target_id'];
    $department_entity = taxonomy_term_load($department_id);
    $form['field_department']['widget']['#options'] = [$department[0]['target_id'] => $department_entity->label()];
}
/**
 * Replace categories with children of the department that the user belongs to.
 *
 * @param array $form
 */
function _domain_limited_categories_replace_categories(&$form) {
  $department = _domain_get_users_department();
  if($department) {
    $target_id = $department[0]['target_id'];
    $vid = 'departments';
    $sub_categories = \Drupal::service('entity_type.manager')
      ->getStorage("taxonomy_term")
      ->loadTree($vid, $parent = $target_id, $max_depth = NULL, $load_entities = FALSE);
    if($sub_categories) {
      $options = [];
      foreach($sub_categories as $category) {
        $options[$category->tid] = $category->name;
      }
      $form['field_categories']['widget']['#options'] = $options;
    } else {
        $form['field_categories']['#access'] = FALSE;
    }
  }
}

function _domain_limited_categories_fieldset(&$form) {

    $form['revision_information']['#weight'] = 55;

    $form['note'] = array(
    '#type' => 'item',
    '#title' => 'Add a note to your CONTENT',
    '#weight' => 54,


  );

    $form['#attached']['library'][] = 'domain_limited_categories/domain_limited_categories';

}


