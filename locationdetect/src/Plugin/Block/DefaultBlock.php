<?php

namespace Drupal\locationdetect\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DefaultBlock' block.
 *
 * @Block(
 *  id = "default_block",
 *  admin_label = @Translation("Default block"),
 * )
 */
class DefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $ip = $_SERVER['REMOTE_ADDR'];

    $currentDomain = $_SERVER['SERVER_NAME'];
    $currentUrl = $_SERVER['REQUEST_URI'];
    $country_code = ip2country_get_country($ip);

	$lang = $country_code == 'GB' ? 'UK'
	  : $country_code == 'US' ? 'US'
	  : '';

	$url = $country_code == 'GB' ? 'www.domain.co.uk'
	  : $country_code == 'US' ? 'www.domain.us'
	  : '';

	$correctLocation = $url == $currentDomain ? true : false;

	$country_code == 'GB' ? $lang = 'UK' : $lang = 'US';
	!empty($lang) ? $string = 'Your from the ' . $lang . ', visit our <a href="http://' . $url.$currentUrl .'">' . $lang . '</a> Site' : '';

  	$build = [];
    $build['default_block']['#markup'] = $correctLocation ? 'Correct Location' : $string;

    return $build;
  }

}
