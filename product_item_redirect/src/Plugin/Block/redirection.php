<?php

namespace Drupal\product_item_redirect\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a 'redirection' block.
 *
 * @Block(
 *  id = "redirection",
 *  admin_label = @Translation("Redirection"),
 * )
 */
class redirection extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $node = \Drupal::routeMatch()->getParameter('node');
	$ref = $node->get('field_product_category_reference')->getValue();

	$nid = $ref[0]['target_id'];

	$options = ['absolute' => TRUE];
	$url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid], $options);
	$url = $url->toString();
	$query = $_GET['nid'];
	$url = $url.'?nid='.$query;
	$response = new RedirectResponse($url);
	$response->send();
	return;

  }
	

}
