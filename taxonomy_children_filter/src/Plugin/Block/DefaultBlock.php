<?php

namespace Drupal\taxonomy_children_filter\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DefaultBlock' block.
 *
 * @Block(
 *  id = "default_block",
 *  admin_label = @Translation("Default block"),
 * )
 */
class DefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity_type = 'article';
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', $entity_type);
    $entity_ids = $query->execute();
    $output = '';
    
    $catId = [];
    $depId = [];
    foreach ($entity_ids as $entity) {
        $node_storage = \Drupal::entityManager()->getStorage('node');
        $load = $node_storage->load($entity);
        $cats = $load->get('field_categories')->getValue();
        $department = $load->get('field_department')->getValue();
        $entitytid = $cats[0]['target_id'];
        $enttid = $department[0]['target_id'];
        $catId[] = $entitytid;
        $depId[] = $enttid;
    }
    
  
    $counters = array_count_values($catId);
    $depcounters = array_count_values($depId);
    
 
    $tid = \Drupal::routeMatch()->getRawParameter('taxonomy_term');
    
    
    if($tid) {
        $parentTids = \Drupal::entityManager()->getStorage('taxonomy_term')->loadAllParents($tid);
        $term = taxonomy_term_load($tid);
    
    
        foreach ($parentTids as $tids) {
            $parent = $tids->id();
        } 
        
        $parentClass = 'category-' . $parent;
        
        !empty($depcounters[$parent]) ? $totalcount = $depcounters[$parent] : $totalcount = 0;
        
      
        $sub_categories = \Drupal::service('entity_type.manager')
          ->getStorage("taxonomy_term")
          ->loadTree('departments', $parent, $max_depth = NULL, $load_entities = FALSE);
    
    
        $tid == $parent ? $active = 'active' : $active = 'not-active';
        $url = '/taxonomy/term/' . $parent;
        $path = \Drupal::service('path.alias_manager')->getAliasByPath($url);
        $output = '<a class="' . $active . ' ' . $parentClass . '" href="' . $path . '">All Categories (' . $totalcount . ')</a>';
        if($sub_categories) {
            $counter = 0;
            foreach ($sub_categories as $cat) {
                $url = '/taxonomy/term/' . $cat->tid;
                $path = \Drupal::service('path.alias_manager')->getAliasByPath($url);
                !empty($counters[$cat->tid]) ? $counter = $counters[$cat->tid] : $counter = 0;
                $tid == $cat->tid ? $active = 'active' : $active = 'not-active';
                $output .= '<a class="' . $active . ' ' . $parentClass . '" href="' . $path . '">' . $cat->name . ' (' . $counter . ')</a>';
            }
        }
        
    }

    return [
        '#markup' => $output,
        '#cache' => [
            'contexts' => ['url']
        ]
    ];

  }
}
