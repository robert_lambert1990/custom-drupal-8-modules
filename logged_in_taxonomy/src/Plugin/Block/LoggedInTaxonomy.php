<?php

namespace Drupal\logged_in_taxonomy\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'LoggedInTaxonomy' block.
 *
 * @Block(
 *  id = "logged_in_taxonomy",
 *  admin_label = @Translation("Logged in taxonomy"),
 * )
 */
class LoggedInTaxonomy extends BlockBase {

  /**
   * {@inheritdoc}
   */

public function build() {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $name = $user->get('name')->value;
    $department = $user->get('field_department')->getValue();
    if($department){
        $tid = $department[0]['target_id'];
        if(isset($tid)) {
            $term = taxonomy_term_load($tid);
        }
        if(isset($term)){
            $term_name = $term->label();
            $term_id = $term->id();
        }

        return [
        	'#theme' => 'logged_in_taxonomy',
        	'#logged_in_user' => $name,
        	'#taxonomy_name' => $term_name,
        	'#taxonomy_id' => $term_id,
            ];
        }
    }
}
