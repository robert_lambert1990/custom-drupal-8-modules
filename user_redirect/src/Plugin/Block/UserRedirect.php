<?php

namespace Drupal\user_redirect\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'UserRedirect' block.
 *
 * @Block(
 *  id = "user_redirect",
 *  admin_label = @Translation("User redirect"),
 * )
 */
class UserRedirect extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['user_redirect']['#markup'] = 'Implement UserRedirect.';

    return $build;
  }

}
